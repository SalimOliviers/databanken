use ModernWays;

-- Tabel Personen aanmaken
drop table if exists Personen;
create table Personen (
    Voornaam varchar(255) not null,
    Familienaam varchar(255) not null
);

-- In de tabel Personen data inserten van de kolommen voornaam, familienaam uit de tabel boeken
insert into Personen (Voornaam, Familienaam)
select distinct Voornaam, Familienaam from Boeken;

-- Extra kolommen toevoegen bij Personen
alter table Personen add (
   Id int auto_increment primary key,
   AanspreekTitel varchar(30) null,
   Straat varchar(80) null,
   Huisnummer varchar (5) null,
   Stad varchar (50) null,
   Commentaar varchar (100) null,
   Biografie varchar(400) null);

-- Kolom toevoegen aan boeken
alter table Boeken add Personen_Id int not null;

-- Id van Personen inserten in de kolom personen_Id van boeken bij de juiste auteur
update Boeken cross join Personen
    set Boeken.Personen_Id = Personen.Id
where Boeken.Voornaam = Personen.Voornaam and
    Boeken.Familienaam = Personen.Familienaam;

-- Kolommen voornaam, familienaam verwijderen uit boeken
alter table Boeken drop column Voornaam,
    drop column Familienaam;

-- Foreign key maken van de kolom personen_id omdat dit een verwijzing is naar de tabel personen
alter table Boeken add constraint fk_Boeken_Personen
   foreign key(Personen_Id) references Personen(Id);