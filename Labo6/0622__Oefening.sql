use aptunes;

create index idx_VoornaamFamilienaam
on muzikanten(Voornaam(10),Familienaam(10));