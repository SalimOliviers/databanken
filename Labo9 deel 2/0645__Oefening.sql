USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleaseWithSuccess`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `MockAlbumReleaseWithSuccess`(out success boolean)
BEGIN
	declare numberOfAlbums INT default 0;
    declare numberOfBands INT default 0;
    declare randomAlbumId INT default 0;
    declare randomBandId INT default 0;
	select count(*) into numberOfAlbums 
    from albums;
    select count(*) into numberOfBands
    from bands;
    select FLOOR(RAND() * numberOfAlbums) + 1 into randomAlbumId;
    select FLOOR(RAND() * numberOfBands) + 1 into randomBandId;
    IF (select not exists (select * from albumreleases where Bands_Id = randomBandId and Albums_Id = randomAlbumId)) THEN
		insert into albumreleases values (randomBandId, randomAlbumId);
        set success = 1;
	ELSE
		set success = 0;
    END IF;
END$$

DELIMITER ;