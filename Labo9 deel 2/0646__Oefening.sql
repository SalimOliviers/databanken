USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleases`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `MockAlbumReleases`(in extraReleases INT)
BEGIN
	declare counter INT default 0;
	
    REPEAT
		call MockAlbumReleaseWithSuccess(@success);
        if (select @success = 1) then
			set counter = counter + 1;
		end if;
    UNTIL counter = extraReleases
    END REPEAT;
END$$

DELIMITER ;