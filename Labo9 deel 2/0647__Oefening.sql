USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleasesLoop`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `MockAlbumReleasesLoop`(in extraReleases INT)
BEGIN
	declare counter INT default 0;
	
    create_Albums:	LOOP
		call MockAlbumReleaseWithSuccess(@success);
        if (select @success = 1) then
			if (counter = extraReleases) then
				leave create_Albums;
			else
				set counter = counter + 1;
			end if;
		end if;
    END LOOP;
END$$

DELIMITER ;