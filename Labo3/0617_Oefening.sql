create view AuteursBoekenRatings
as
select Auteur, Titel, Rating from AuteursBoeken ab
inner join GemiddeldeRatings gr on ab.Boeken_Id = gr.Boeken_Id