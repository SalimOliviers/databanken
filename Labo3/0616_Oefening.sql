create view GemiddeldeRatings
as
select Boeken_Id, AVG(Rating) as Rating from boeken
inner join reviews on boeken.Id = reviews.Boeken_Id
group by Boeken_Id;