create view AuteursBoeken
as
select concat(personen.Voornaam,' ', personen.Familienaam) as "Auteur", boeken.Titel from boeken
inner join publicaties on boeken.Id = publicaties.Boeken_Id
inner join personen on publicaties.Personen_Id = personen.Id;