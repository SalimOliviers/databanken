use TussentijdseEvaluatie1BDB;

select COALESCE (concat(Voornaam, " ", Familienaam), "is nog nooit door iemand uitgeleend") as "Volledige naam", COALESCE (Titel, 'heeft nog nooit iets uitgeleend') as Titel from ontleningen
right join boeken on ontleningen.Boeken_Id = boeken.Id
left join leden on ontleningen.Leden_Id = leden.Id
where ontleningen.Boeken_Id is null
union all
select COALESCE (concat(Voornaam, " ", Familienaam), "is nog nooit door iemand uitgeleend") as "Volledige naam", COALESCE (Titel, 'heeft nog nooit iets uitgeleend') as Titel from ontleningen
right join boeken on ontleningen.Boeken_Id = boeken.Id
right join leden on ontleningen.Leden_Id = leden.Id
where ontleningen.Boeken_Id is null;