use TussentijdseEvaluatie1BDB;

select Titel, COncat(Voornaam, " ", Familienaam) as Naam from boeken
inner join ontleningen on boeken.Id = ontleningen.Boeken_Id
inner join leden on ontleningen.Leden_Id = leden.Id;
