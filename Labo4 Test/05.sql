use TussentijdseEvaluatie1BDB;

select Titel, count(*) from boeken
inner join ontleningen on boeken.Id = ontleningen.Boeken_Id
group by Titel
having count(*) >= 3;