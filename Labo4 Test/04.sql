use TussentijdseEvaluatie1BDB;

create view BoekenInfo
as
select Titel, Concat(Voornaam, " ", Familienaam) as "Naam auteur", Naam as "Naam uitgeverij"from boeken
inner join auteurs on boeken.Auteurs_Id = auteurs.Id
inner join uitgeverijen on boeken.Uitgeverijen_Id = uitgeverijen.Id;