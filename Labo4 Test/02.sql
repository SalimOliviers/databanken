use TussentijdseEvaluatie1BDB;

create view OnpopulaireBoeken
as
select Titel from boeken
left join ontleningen on boeken.Id = ontleningen.Boeken_Id
where ontleningen.Boeken_Id IS null;