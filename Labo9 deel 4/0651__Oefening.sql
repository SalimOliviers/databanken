USE `aptunes`;
DROP procedure IF EXISTS `GetAlbumDuration`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetAlbumDuration`(in albumId INTEGER, out totalDuration smallint unsigned)
BEGIN
	declare ok INTEGER DEFAULT 0;
    declare songDuration tinyint unsigned;
    
    declare currentSong
    cursor for select Lengte from liedjes inner join albums on liedjes.Albums_Id = albums.Id where Albums_Id = albumId;
    
    declare continue handler
    for NOT FOUND set ok = 1;
    
    open currentSong;
    
    set totalDuration = 0;
    getSongLength: LOOP
			FETCH currentSong into songDuration;
		if ok = 1
        then
				leave getSongLength;
			end if;
		set totalDuration = totalDuration + songDuration;
        END LOOP getSongLength;
        
	close currentSong;
    
END$$

DELIMITER ;