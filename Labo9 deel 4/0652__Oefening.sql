create user student@localhost
identified by 'ikbeneenstudent';

grant execute on procedure aptunes.GetAlbumDuration
to student@localhost;