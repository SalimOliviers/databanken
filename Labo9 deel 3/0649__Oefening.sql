USE `aptunes`;
DROP procedure IF EXISTS `DemonstrateHandlerOrder`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `DemonstrateHandlerOrder`()
BEGIN
	declare rnd INT default 0;
	declare exit handler for sqlstate '45001'
    begin
		RESIGNAL SET MESSAGE_TEXT = 'Ik heb mijn best gedaan!';
    end;
    declare continue handler for sqlstate '45002'
    begin
		select 'Een algemene fout opgevangen.';
    end;
    declare exit handler for sqlstate '45003'
    begin
		RESIGNAL SET MESSAGE_TEXT = 'Ik heb mijn best gedaan!';
    end;
	
    SELECT floor((RAND()* 3)+1) into rnd;
    
	if rnd = 1 then
		signal sqlstate '45001';
    ELSEIF rnd = 2 then
		signal sqlstate '45002';
    else
		signal sqlstate '45003';
	end if;
    
END$$

DELIMITER ;