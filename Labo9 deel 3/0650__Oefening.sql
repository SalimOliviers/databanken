USE `aptunes`;
DROP procedure IF EXISTS `DangerousInsertAlbumreleases`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `DangerousInsertAlbumreleases`()
BEGIN
	declare counter INT default 0;
	declare numberOfAlbums INT default 0;
    declare numberOfBands INT default 0;
    declare randomAlbumId INT default 0;
    declare randomBandId INT default 0;
	declare exit handler for sqlstate '45000'
    begin
		rollback;
		RESIGNAL SET MESSAGE_TEXT = '"Nieuwe releases konden niet worden toegevoegd.';
    end;
    
	select count(*) into numberOfAlbums 
    from albums;
    select count(*) into numberOfBands
    from bands;
    
    start transaction;
		loop_addAlbumreleases: LOOP
			set counter = counter +1;
            select FLOOR(RAND() * numberOfAlbums) + 1 into randomAlbumId;
			select FLOOR(RAND() * numberOfBands) + 1 into randomBandId;
			if counter <= 3 then
				IF (select not exists (select * from albumreleases where Bands_Id = randomBandId and Albums_Id = randomAlbumId)) THEN
					insert into albumreleases values (randomBandId, randomAlbumId);
				else
					signal sqlstate '45000';
				END IF;
			else
				leave loop_addAlbumreleases;
			end if;
		end loop;
    commit;
END$$

DELIMITER ;