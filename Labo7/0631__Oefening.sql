use modernways;

select distinct(Voornaam) from studenten
where Voornaam IN (select Voornaam from personeelsleden) 
   && Voornaam IN (select Voornaam from directieleden);