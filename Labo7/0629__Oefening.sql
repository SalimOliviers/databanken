use modernways;

select Id from studenten
inner join evaluaties on studenten.Id = evaluaties.Studenten_Id
group by Studenten_Id
having avg(Cijfer) > (select avg(Cijfer) from evaluaties);