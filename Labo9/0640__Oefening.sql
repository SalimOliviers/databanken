USE `aptunes`;
DROP procedure IF EXISTS `GetLiedjes`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetLiedjes`(IN lyric VARCHAR(50))
BEGIN
    SELECT Titel
    FROM liedjes
    WHERE Titel LIKE CONCAT('%', lyric, '%');
END$$

DELIMITER ;