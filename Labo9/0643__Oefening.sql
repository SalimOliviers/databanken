USE `aptunes`;
DROP procedure IF EXISTS `CreateAndReleaseAlbum`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `CreateAndReleaseAlbum`(in titel VARCHAR(100), in bands_Id INT)
BEGIN
	start transaction;
    insert into albums (Titel) values (titel);
    insert into albumreleases values (bands_Id, last_insert_id());
    commit;
END$$

DELIMITER ;