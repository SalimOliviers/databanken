USE `aptunes`;
DROP procedure IF EXISTS `NumberOfGenres`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `NumberOfGenres`(out totaal TINYINT)
BEGIN
    SELECT count(*)
    into totaal
	FROM genres;
END$$

DELIMITER ;