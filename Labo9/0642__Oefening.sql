USE `aptunes`;
DROP procedure IF EXISTS `CleanupOldMemberships`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `CleanupOldMemberships`(in datum datetime, out cleaned INT)
BEGIN
	select count(*) into cleaned 
    from lidmaatschappen 
    where Einddatum < datum
    and Einddatum is not null;
    
    delete from lidmaatschappen
    where Einddatum < datum
    and Einddatum is not null;
END$$

DELIMITER ;